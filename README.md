# Docker-compose gitlab ci project

## Project description
This is a simple project for trying out gitlab-ci. 

## Project usage
___

### 1. Normal usage
- From the git root folder change into project folder.
```
$ cd project/
```
- This folder includes 2 docker-compose files. 1 is normal usage and 1 is for testing. Only difference between these 2 files is that docker-compose.test.yml has additional test container for running tests.
- Start the normal version of the project with:
```
$ docker-compose up --build
```
- service_1 now answers in port 8001

### 2. Test usage
- You can run the test version with: 
```
$ docker-compose -f docker-compose.test.yml up --build
```
- The test version runs several tests and shuts down the containers using the /shutdown feature.

### 3. Features
- The project has the following features:
    * GET /
        - Returns hello message with information about the services
        - Example:
        ```
        $ curl -s http://localhost:8001
        Hello from 192.168.99.1:51769 to 172.30.0.2:80. Hello from 172.30.0.2:49118 to 172.30.0.3:80.
        ```
    * POST /fibo
        - Returns the Fibonacci number that the user requests.
        - To receive proper response the payload should include {number: int} in the body.
        - Example:
        ```
         $ curl -sd "number=12" -X POST http://localhost:8001/fibo
         144
        ```
    * GET /run-log
        - Returns log data of boots and shutdowns of the system.
        - Example:
        ```
        $ curl -s http://localhost:8001/run-log
        BOOT 2000-12-24 12:00:59.967018
        SHUTDOWN 2010-12-24 12:00:00.132761
        BOOT 2019-11-27 10:36:44.936088
        ```
    * POST /shutdown
        - Shuts down the services
        - Example:
        ```
        $ curl -s -X POST http://localhost:8001/fibo
        Shutting down service
        ```




## Gitlab ci usage
___

### 1. Start local Gitlab
- From the git root folder change into gitlab folder.
```
$ cd gitlab/
```
- Start the Gitlab.
```
$ docker-compose up
```
- Wait for Gitlab to start properly. This will take few minutes.


### 2. Set up the project
- Open your web browser and go to http://localhost:8080/root/.
- Set up new password for your root user.
- Log in to your local gitlab (username: root).
- Create new project and add it as a remote to the project.
- Example of creating remote named "local" using project named "project":
```
$ git remote add local http://localhost:8080/root/project.git
$ git remote set-url --add --push local http://localhost:8080/root/project.git
```


### 3. Get your token and start the runner
- Open your web browser and go to http://localhost:8080/root/(project-name)/settings/ci_cd
- Expand the "Runners" section and copy the runner registration token.
- Open "gitlab-runner-register&#46;sh" and paste your token to registration_token.
- Register gitlab runner using the script.
```
$ ./gitlab-runner-register.sh
```
- If you receive the following error the service has not yet started properly so wait a litte bit more and try again.
```
ERROR: Registering runner... failed
```


### 4. Using the gitlab ci pipeline
- Push the project to the local gitlab.
- Example using the setup from earlier:
```
$ git push local master
```
- This will start the pipeline you can see by visiting http://localhost:8080/root/(project-name)/pipelines
