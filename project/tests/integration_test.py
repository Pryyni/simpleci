import pytest
import requests
from urllib.parse import urljoin
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter


@pytest.fixture(scope="function")
def wait_for_containers():
    """Wait for docker containers to be responsive"""
    request_session = requests.Session()
    service_addr = "http://service_1"
    retries = Retry(total=5,
                    backoff_factor=0.3,
                    status_forcelist=[500, 502, 503, 504])
    request_session.mount('http://', HTTPAdapter(max_retries=retries))

    assert request_session.get(service_addr)
    return request_session, service_addr


def test_hello(wait_for_containers):
    request_session, service = wait_for_containers

    res = request_session.get(
        url=service
    )
    assert res.status_code == requests.codes.ok
    assert res.text.startswith('Hello from')


def test_fibo(wait_for_containers):
    request_session, service = wait_for_containers

    # Not a number test
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'20-'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "Error: Not a number"

    # Negative number
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'-1'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "Error: Negative number"

    # Zero
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'0'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "0"

    # 1
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'1'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "1"

    # 2
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'2'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "1"

    # 3
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'3'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "2"

    # Larger number
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'20'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "6765"

def test_fibo_payload(wait_for_containers):
    request_session, service = wait_for_containers

    # No payload
    res = request_session.post(
        url=urljoin(service, 'fibo')        
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "Error: Not a number"

    # No number in payload
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'dumber':'20'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "Error: Not a number"

    # Additional data in payload
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'12', 'extra':'number'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "144"

    # Float in payload
    res = request_session.post(
        url=urljoin(service, 'fibo'),
        data={'number':'12.0'}
    )
    assert res.status_code == requests.codes.ok
    assert res.text == "Error: Not a number"



def test_get_log(wait_for_containers):
    request_session, service = wait_for_containers

    res = request_session.get(
        url=urljoin(service, 'run-log')
    )

    # Verify succesful request
    assert res.status_code == requests.codes.ok

    # Make sure that the log starts with BOOT status
    assert res.text.startswith('BOOT')

#Always run this test last
def test_shutdown(wait_for_containers):
    request_session, service = wait_for_containers

    res = request_session.post(
        url=urljoin(service, 'shutdown')
    )

    # Verify succesful request
    assert res.status_code == requests.codes.ok

    # Check that the server is not responding anymore
    try:
        res = request_session.get(
            url=service
        )
    except:
        pass
    else:
        # If no exceptions was raised shutdown did not work
        assert False

    