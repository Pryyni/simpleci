import sys
import socket
import requests
import datetime
from flask import Flask, request
app = Flask(__name__)


@app.route('/', methods=['GET'])
def hello():
    """Responds with hello message"""

    # Get <IP>:<Port> data for remote
    remote = '%s:%s' % (request.environ.get('REMOTE_ADDR'), request.environ.get('REMOTE_PORT'))

    # Get <IP>:<Port> data for local
    local = '%s:%s' % (socket.gethostbyname(socket.gethostname()), request.environ.get('SERVER_PORT'))

    # Send the request to service 2 and get their response
    s2res = requests.get('http://service_2')

    # Create and respond to the request
    return 'Hello from %s to %s. %s.' % (remote, local, s2res.text)


@app.route('/fibo', methods=['POST'])
def fibo():
    """Calculates the Fibonacci number

        Payload:
            number: integer in string format that corresponds the Fibonacci number the user wants.
    
    """
    # Check if the payload can be converted into number
    try:
        number = int(request.form.get("number"))
    except ValueError:
        # Value of number is not integer
        return "Error: Not a number"
    except TypeError:
        # No number in the payload
        return "Error: Not a number"

    # Fibonacci numbers don't exist to negative numbers
    if number < 0:
        return "Error: Negative number"

    # Calculate the Fibonacci number
    else:
        a = 0
        b = 1
        for i in range(number):
            a, b = b, a+b
        return str(a)




@app.route('/run-log', methods=['GET'])
def getlog():
    """Retrieves the log data"""

    # Open the log file
    with open("/log/logfile.txt", "r") as logfile:
        logdata = logfile.read()

    return logdata


@app.route('/shutdown', methods=['POST'])
def shutdown():
    """Shuts down both services"""

    # Shutdown service 2
    res = requests.post(
        url='http://service_2/shutdown'
    )
    # Add shutdown to the log
    log("SHUTDOWN")

    # Shut down the Flask server
    shut = request.environ.get('werkzeug.server.shutdown')

    # If using werkzeug shutdown doesn't work, use exception
    if shut is None:
        raise RuntimeError('Shutting down server')

    # Run shutdown
    shut()

    # Respond to the request
    return "Shutting down service"

def log(event: str):
    """logs events into the persistent log"""

    #Create or open file
    with open("/log/logfile.txt", "a") as logfile:
        logfile.write("%s %s\n" % (event, datetime.datetime.now()))


if __name__ == "__main__":
    log("BOOT")
    try:
        app.run(host='0.0.0.0', port=80)
    except RuntimeError as e:
        if str(e) == 'Shutting down server':
            pass

