import socket
import requests
from flask import Flask, request
app = Flask(__name__)


@app.route('/', methods=['GET'])
def hello():
    """Responds with hello message"""
    remote = '%s:%s' % (request.environ.get('REMOTE_ADDR'), request.environ.get('REMOTE_PORT'))
    local = '%s:%s' % (socket.gethostbyname(socket.gethostname()), request.environ.get('SERVER_PORT'))
    return 'Hello from %s to %s' % (remote, local)

@app.route('/shutdown', methods=['POST'])
def shutdown():
    """Shuts down service 2"""
    shut = request.environ.get('werkzeug.server.shutdown')
    if shut is None:
        raise RuntimeError('Shutting down server')
    shut()
    return "Shutting down service 2"

if __name__ == "__main__":
    try:
        app.run(host='0.0.0.0', port=80)
    except RuntimeError as e:
        if str(e) == 'Shutting down server':
            pass

